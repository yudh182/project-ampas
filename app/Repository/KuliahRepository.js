'use strict'

const Database = use('Database')
const Kuliah = use('App/Models/Kuliah')
const Hashids = require('hashids/cjs')
const hashids = new Hashids()

class KuliahRepository {
    async getAll({request, params, response}) {
        let resultQuery = [];
        var no = 1;
        
        let all_column = [
            'kuliah_id',
            'mapel_name',
            'dosen_name'
        ];

        let myQuery = Database
                        .select(all_column)
                        .from('kuliah')
                        .leftJoin('mapels', 'kuliah.kuliah_mapel_id', 'mapels.mapel_id')
                        .leftJoin('dosen', 'kuliah.kuliah_dosen_id', 'dosen.dosen_id');
        
        if (typeof request.only(['size']).size !== 'undefined') {
            let offset = (request.only(['page']).page * request.only(['size']).size) - request.only(['size']).size;
            myQuery.limit(request.only(['size']).size);
            myQuery.offset(offset);
        } else {
            myQuery.limit(request.only(['size']).size);
        }

        if (typeof request.only(['search']).search !== 'undefined' && all_column.indexOf(request.only(['search']).search)) {
            myQuery.where('mapel_name', 'like', '%'+request.only(['search']).search+'%');
            myQuery.orWhere('dosen_name', 'like', '%'+request.only(['search']).search+'%');
        }

        if (typeof request.only(['column']).column !== 'undefined' && all_column.indexOf(request.only(['column']).column)) {
            if (typeof request.only(['sort']).sort !== 'undefined') {
                myQuery.orderBy(request.only(['column']).column, request.only(['sort']).sort);
            } else {
                myQuery.orderBy(request.only(['column']).column);
            }
        } else {
            myQuery.orderBy(1, 'desc');
        }

        if(typeof request.only(['size']).size !== 'undefined') {
            no = (request.only(['size']).size * request.only(['page']).page) - (request.only(['size']).size - 1);
        }

        resultQuery = await myQuery;

        for(let i in resultQuery) {
            resultQuery[i].kuliah_id = hashids.encode(resultQuery[i].kuliah_id);
            resultQuery[i].no = no;
            no++;
        }

        return resultQuery;
    }

    async getDropdown() {
        let all_column = [
            'kuliah_id as id',
            'kuliah_dosen_id as text'
        ];
        
        let myQuery = Database.select(all_column).from('kuliah');
        
        return await myQuery;
    }
}

module.exports = KuliahRepository