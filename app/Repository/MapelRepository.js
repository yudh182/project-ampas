'use strict'

const Database = use('Database')

// model atau skema
const Mapel = use('App/Models/Mapel')


class MapelRepository {
    async getAllMapel() {
        // all logic with db here
        // example if you want convert from date to date db format 
        // moment(now()).format('/)
        return await Mapel.query().fetch();
    }
}

module.exports = MapelRepository
