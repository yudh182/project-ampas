'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Kuliah extends Model {
	static get table () {
		return 'mapel_dosen'
	}
	
	static get primaryKey () {
		return 'mapeldosen_id'
	}
}

module.exports = Kuliah