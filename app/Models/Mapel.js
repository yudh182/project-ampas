'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mapel extends Model {
	static get table () {
		return 'mapels'
	}
	
	static get primaryKey () {
		return 'id'
	}
}

module.exports = Mapel
