'use strict'
const md5 = require('md5');
const User = use('App/Models/User')

class AuthController {
    async login ({ request, auth, response}) {
        const { email, password } = request.all()
        let resultQuery =  await auth
            .authenticator('jwt')
            .withRefreshToken()
            .attempt(email, password)

        let result = {
            'status' 	: true,
            'msg'		: 'Success',
            'data'		: resultQuery 
        }
        return result;
    }

    async profile ({ response, auth }) {
        let resultQuery = response.send(auth.current.user)
        
        let result = {
            'status' 	: true,
            'msg'		: 'Success',
            'data'		: resultQuery 
        }
        return result;
    }
    
    async refresh ({ request, auth }) {
        const refreshToken = request.input('refresh_token')
        let resultQuery = await auth
            .newRefreshToken()
            .generateForRefreshToken(refreshToken)

        let result = {
			'status' 	: true,
			'msg'		: 'Success',
			'data'		: resultQuery 
		}
		return result;
    }
    
    async logout ({ auth, response }) {
        const apiToken = auth.getAuthHeader()
        await auth
            .authenticator('jwt')
            .revokeTokens([apiToken])
        return response.send({ message: 'Logout successfully!'})
    }
}

module.exports = AuthController
