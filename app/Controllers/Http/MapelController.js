'use strict'

// core

const { validate } = use('Validator')
// ketika menggunakan metode repository maka haram hukumnya ada query dalam controller atau delivery
const Database = use('Database')
const Mapel = use('App/Models/Mapel')
const Hashids = require('hashids/cjs')
const hashids = new Hashids()

// repository
const MapelRepository = new (use('App/Repository/MapelRepository'))

class MapelController {
	async getAll({ request, params, auth, response }) {
		const result = await MapelRepository.getAllMapel();

		return response.status(200).json({
			'status': true,
			'msg': 'Success',
			'data': result
		})
	}

	async index ({request, params, response}) {
		let resultQuery = [];
		var no = 1;
		if (typeof request.only(['is_dropdown']).is_dropdown == 'undefined') {
			let all_column = [
				'mapel_id',
				'mapel_code',
				'mapel_name'
			];

			let myQuery = Database.select(all_column).from('mapels');

			if (typeof request.only(['size']).size !== 'undefined') {
				let offset = (request.only(['page']).page * request.only(['size']).size) - request.only(['size']).size;
				myQuery.limit(request.only(['size']).size);
				myQuery.offset(offset);
			} else {
				myQuery.limit(request.only(['size']).size);
			}

			if (typeof request.only(['search']).search !== 'undefined' && all_column.indexOf(request.only(['search']).search)) {
				myQuery.where('mapel_code', 'like', '%'+request.only(['search']).search+'%');
				myQuery.orWhere('mapel_name', 'like', '%'+request.only(['search']).search+'%');
			}

			if (typeof request.only(['column']).column !== 'undefined' && all_column.indexOf(request.only(['column']).column)) {
				if (typeof request.only(['sort']).sort !== 'undefined') {
					myQuery.orderBy(request.only(['column']).column, request.only(['sort']).sort);
				} else {
					myQuery.orderBy(request.only(['column']).column);
				}
			} else {
				myQuery.orderBy(1, 'desc');
			}

			if(typeof request.only(['size']).size !== 'undefined') {
				no = (request.only(['size']).size * request.only(['page']).page) - (request.only(['size']).size - 1);
			}

			resultQuery = await myQuery;

			for(let i in resultQuery) {		// if (request with params only)
				resultQuery[i].mapel_id = hashids.encode(resultQuery[i].mapel_id);
				resultQuery[i].no = no;
				no++;
			}
		} else {
			let all_column = [
				'mapel_id as id',
				'mapel_name as text'
			];
			
			let myQuery = Database.select(all_column).from('mapels');

			resultQuery = await myQuery;
		}

		let result = {
			'status' 	: true,
			'msg'		: 'Success',
			'data'		: resultQuery 
		}
		return result;
	}

	async detail ({params, response}) {
		const mapel = await Mapel.find(params.id)
		return response.json(mapel)
	}

	async input({request, response}) {
		const rules = {
		  mapel_code: 'required',
		  mapel_name: 'required'
		}

		const validation = await validate(request.all(), rules)

		if (validation.fails()) {
			return await validation.messages();
		} else {
			const mapelInfo = request.only(['mapel_code', 'mapel_name'])
			const mapel = new Mapel()
			mapel.mapel_code = mapelInfo.mapel_code
			mapel.mapel_name = mapelInfo.mapel_name
			await mapel.save()
			return response.status(201).json(mapel)
		}
	}

	async update ({params, request, response}) {
		const mapelInfo = request.only(['mapel_code', 'mapel_name'])
		const mapel = await Mapel.find(params.id)
		if (!mapel) {
			return response.status(404).json({data: 'Resource not found'})
		}
		mapel.mapel_code = mapelInfo.mapel_code
		mapel.mapel_name = mapelInfo.mapel_name
		await mapel.save()
		return response.status(200).json(mapel)
	}

	async delete({params, response}) {				// message not active
		const mapel = await Mapel.find(params.id)
		if (!mapel) {
			return response.status(404).json({data: 'Resource not found'})
		}
		await mapel.delete()
		return response.status(204).json(null)
	}
}

module.exports = MapelController
