'use strict'

const { validate } = use('Validator')
const Database = use('Database')
const Kuliah = use('App/Models/Kuliah')
const Hashids = require('hashids/cjs')
const hashids = new Hashids()

const KuliahRepository = new (use('App/Repository/KuliahRepository'))

class KuliahController {
	async index ({request, params, response}) {
		let resultQuery = [];
		if (typeof request.only(['is_dropdown']).is_dropdown == 'undefined') {
			resultQuery = await KuliahRepository.getAll({request, params, response});
		} else {
			resultQuery = await KuliahRepository.getDropdown();
		}

		let result = {
			'status' 	: true,
			'msg'		: 'Success',
			'data'		: {
				'count': resultQuery.length,
				'data' : resultQuery
			}
		}
		return result;
	}
}

module.exports = KuliahController