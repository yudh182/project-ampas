'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MapelSchema extends Schema {
  up () {
    this.create('mapels', (table) => {
      	table.increments()
		table.string('mapel_code').nullable()
		table.string('mapel_name').nullable()
      	table.timestamps()
    })
  }

  down () {
    this.drop('mapels')
  }
}

module.exports = MapelSchema
