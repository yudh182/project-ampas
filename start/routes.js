'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(() => {
	// contoh penggunaan repo 
	Route.get('/all', 'MapelController.getAll')

	Route.get('students', 'StudentController.index')
	Route.get('students/:id', 'StudentController.show')
	Route.post('students', 'StudentController.store')
	Route.put('students/:id', 'StudentController.update')
	Route.delete('students/:id', 'StudentController.delete')

	Route.get('mapels', 'MapelController.index').middleware(['auth:jwt'])
	Route.get('mapels/:id', 'MapelController.detail')
	Route.post('mapels', 'MapelController.input')
	Route.put('mapels/:id', 'MapelController.update')
  	Route.delete('mapels/:id', 'MapelController.delete')
  
	Route.post('auth/login', 'AuthController.login')
	Route.post('auth/refresh', 'AuthController.refresh').middleware(['auth:jwt'])
	Route.get('auth/logout', 'AuthController.logout').middleware(['auth:jwt'])
	Route.get('auth/profile', 'AuthController.profile').middleware(['auth:jwt'])

	Route.get('kuliah', 'KuliahController.index')
	Route.get('kuliah/test', 'KuliahController.test')

}).prefix('api/v1')